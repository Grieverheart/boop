#ifndef BOOP_H
#define BOOP_H

#ifdef __cplusplus
struct BoopConfig;
extern "C" {
#else
#include <stdbool.h>
typedef struct BoopConfig BoopConfig;
#endif

BoopConfig* boop_config_new(unsigned int n_part, double* positions, const double box_bounds[3], bool is_periodic, double range);
void boop_config_delete(BoopConfig*);

void boop_ql(const BoopConfig* config, int l, double* qls);
void boop_wl(const BoopConfig* config, int l, double* wls);
void boop_ql_av(const BoopConfig* config, int l, double* qls);
void boop_wl_av(const BoopConfig* config, int l, double* wls);

#ifdef __cplusplus
}
#endif

#endif
