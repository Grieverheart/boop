SRC=$(wildcard *.cpp)
OBJ=$(patsubst %.cpp, %.o, $(SRC))
LIB=libboop.so

CC=g++
CFLAGS=-Wall -O3 -std=c++11 -march=native -DNDEBUG -fPIC
LDFLAGS= -lm
RM=rm

%.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: all
all: $(LIB)
	@echo Done

$(LIB): $(OBJ)
	$(CC) -shared $^ -o $@
	
.PHONY: clean
clean:
	-$(RM) $(OBJ)
	@echo Clean Done!
