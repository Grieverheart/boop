#include "boop_cell_list.h"
#include <cstring>
#include <cstdio>
#include <algorithm>

//NOTE: At some point we should make the box a 3x3 matrix

BoopCellList::BoopCellList(void):
    cell_(nullptr),
    linkedList_(nullptr),
    pCellIds_(nullptr),
    cellNeighbours_(nullptr),
    cellDirNeighbours_(nullptr),
    cellVolNeighbours_(nullptr)
{}

BoopCellList::~BoopCellList(void){
    delete[] cell_;
    delete[] linkedList_;
    delete[] cellNeighbours_;
    delete[] cellDirNeighbours_;
    delete[] cellVolNeighbours_;
    delete[] pCellIds_;
}

void BoopCellList::init(int nPart, clam::Vec3d box_bounds, double minCellSize){

    cellMinSize_ = minCellSize;
    nPart_       = nPart;

    nCellsTot_ = 1;
    clam::Vec3d nCells(box_bounds / minCellSize);
    for(int i = 0; i < 3; ++i){
        // Handle the case where nCells_ < 3. We just need to make it 3!
        nCells_[i]   = std::max(3, int(nCells[i]));
        cellSize_[i] = box_bounds[i] / nCells_[i];
        nCellsTot_ *= nCells_[i];
    }

    cell_              = new int[nCellsTot_];
    linkedList_        = new int[nPart_];
    pCellIds_          = new int[nPart_]{};
    cellNeighbours_    = new int[27 * nCellsTot_];
    cellDirNeighbours_ = new int[54 * nCellsTot_];
    cellVolNeighbours_ = new int[14 * nCellsTot_];


    for(int i = 0; i < nCellsTot_; ++i) cell_[i] = CLL_EMPTY;
    for(int i = 0; i < nPart_; ++i) linkedList_[i] = CLL_EMPTY;

    static const int vol_neigh[] = {11, 13, 14, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26};
    int k = 0;
    for(int n = 0; n < nCellsTot_; ++n){
        int cellIndices[3];
        indexToIndices(n, cellIndices);
        int ds[6] = {0};
        for(int i = 0; i < 27; ++i){
            int offset[] = {i % 3 - 1, (i / 3) % 3 - 1, i / 9 - 1};
            int cell_idx =  (nCells_[0] + cellIndices[0] + offset[0]) % nCells_[0] +
                           ((nCells_[1] + cellIndices[1] + offset[1]) % nCells_[1] +
                           ((nCells_[2] + cellIndices[2] + offset[2]) % nCells_[2]) * nCells_[1]) * nCells_[0];
            for(int d = 0; d < 6; ++d){
                if(offset[d / 2] == 1 - 2 * (d % 2)){
                    cellDirNeighbours_[9 * (6 * n + d) + ds[d]++] = cell_idx;
                }
            }
            cellNeighbours_[27 * n + i] = cell_idx;
            if(i == vol_neigh[k % 14]) cellVolNeighbours_[k++] = cell_idx;
        }
    }
}

BoopCellList::BoopCellList(const BoopCellList& other):
    nPart_(other.nPart_), nCellsTot_(other.nCellsTot_),
    cell_(new int[other.nCellsTot_]), linkedList_(new int[other.nPart_]), pCellIds_(new int[other.nPart_]),
    cellNeighbours_(new int[27 * other.nCellsTot_]), 
    cellDirNeighbours_(new int[54 * nCellsTot_]),
    cellVolNeighbours_(new int[14 * other.nCellsTot_]),
    cellSize_(other.cellSize_), cellMinSize_(other.cellMinSize_)
{
    memcpy(nCells_, other.nCells_, 3 * sizeof(int));
    memcpy(cell_, other.cell_, nCellsTot_ * sizeof(int));
    memcpy(linkedList_, other.linkedList_, nPart_ * sizeof(int));
    memcpy(pCellIds_, other.pCellIds_, nPart_ * sizeof(int));
    memcpy(cellNeighbours_, other.cellNeighbours_, 27 * nCellsTot_ * sizeof(int));
    memcpy(cellDirNeighbours_, other.cellDirNeighbours_, 54 * nCellsTot_ * sizeof(int));
    memcpy(cellVolNeighbours_, other.cellVolNeighbours_, 14 * nCellsTot_ * sizeof(int));
}

//TODO: What about the case where box_bounds < cellMinSize_ anyway?
bool BoopCellList::should_rebuild(const clam::Vec3d& factor)const{
    for(int i = 0; i < 3; ++i){
        if(factor[i] * cellSize_[i] < cellMinSize_ ||
           factor[i] * cellSize_[i] > 1.1 * cellMinSize_) return true;
    }
    return false;
}

void BoopCellList::rescale(const clam::Vec3d& factor){
    cellSize_ *= factor;
}

int BoopCellList::add(int pid, const clam::Vec3d& pos){
    return add(pid, cellIndex(pos));
}

int BoopCellList::add(int pid, int cidx){
    pCellIds_[pid] = cidx;

    linkedList_[pid] = cell_[cidx];
    cell_[cidx] = pid;

    return cidx;
}

int BoopCellList::update(int pid, int cidx){
    if(cidx == pCellIds_[pid]) return cidx;

    int cidx_old = pCellIds_[pid];
    if(cell_[cidx_old] == pid) cell_[cidx_old] = linkedList_[pid];
    else{
        for(int ci = cell_[cidx_old]; ci != CLL_EMPTY; ci = linkedList_[ci]){
            if(linkedList_[ci] == pid){
                linkedList_[ci] = linkedList_[pid];
                break;
            }
        }
    }

    linkedList_[pid] = cell_[cidx];
    cell_[cidx] = pid;
    pCellIds_[pid] = cidx;

    return cidx;
}

int BoopCellList::update(int pid, const clam::Vec3d& pos){
    int cidx = cellIndex(pos);
    return update(pid, cidx);
}

void BoopCellList::clear(void){
    for(int i = 0; i < nPart_; ++i){
        linkedList_[i] = CLL_EMPTY;
        pCellIds_[i] = 0;
    }
    for(int i = 0; i < nCellsTot_; ++i) cell_[i] = CLL_EMPTY;
}

int BoopCellList::move(int pid, int coffset){
    int cidx_old = pCellIds_[pid];
    int cidx = cellDirNeighbours_[54 * pCellIds_[pid] + 9 * coffset + 4];

    if(cell_[cidx_old] == pid) cell_[cidx_old] = linkedList_[pid];
    else{
        for(int ci = cell_[cidx_old]; ci != CLL_EMPTY; ci = linkedList_[ci]){
            if(linkedList_[ci] == pid){
                linkedList_[ci] = linkedList_[pid];
                break;
            }
        }
    }

    linkedList_[pid] = cell_[cidx];
    cell_[cidx] = pid;
    pCellIds_[pid] = cidx;

    return cidx;
}

int BoopCellList::n_cells(void)const{
    return nCellsTot_;
}

int BoopCellList::cell_index(int pid)const{
    return pCellIds_[pid];
}

int BoopCellList::cell_index_at_offset(int cid, int coffset)const{
    return cellDirNeighbours_[54 * cid + 9 * coffset + 4];
}

clam::Vec3d BoopCellList::cell_size(void)const{
    return cellSize_;
}

double BoopCellList::cell_min_size(void)const{
    return cellMinSize_;
}

BoopCellList::CellIterator BoopCellList::cells(void)const{
    return CellIterator(nCellsTot_);
}

BoopCellList::CellNeighbourIterator BoopCellList::cell_nbs(int cid)const{
    return CellNeighbourIterator(cellNeighbours_ + 27 * cid);
}

BoopCellList::CellNeighbourIterator BoopCellList::cell_vol_nbs(int cid)const{
    return CellNeighbourIterator(cellVolNeighbours_ + 14 * cid, 14);
}

BoopCellList::CellNeighbourIterator BoopCellList::particle_cell_nbs(int pid)const{
    return CellNeighbourIterator(cellNeighbours_ + 27 * pCellIds_[pid]);
}

BoopCellList::CellNeighbourIterator BoopCellList::particle_cell_nbs(const clam::Vec3d& pos)const{
    return CellNeighbourIterator(cellNeighbours_ + 27 * cellIndex(pos));
}

BoopCellList::CellNeighbourIterator BoopCellList::cell_dir_nbs(int cid, int dir)const{
    return CellNeighbourIterator(cellDirNeighbours_ + 54 * cid + 9 * dir, 9);
}

BoopCellList::CellContentIterator BoopCellList::cell_content(int cid)const{
    return CellContentIterator(*this, cid);
}

void BoopCellList::push_front(const CellNeighbourIterator& itr){
    int* neigh = &cellNeighbours_[itr.cellNeighbours_ - cellNeighbours_];
    int temp = neigh[itr.nidx_];
    memmove(neigh + 1, neigh, itr.nidx_ * sizeof(int));
    neigh[0] = temp;
}

int BoopCellList::indicesToIndex(const int (&indices)[3])const{
    return indices[0] + (indices[1] + indices[2] * nCells_[1]) * nCells_[0];
}

void BoopCellList::indexToIndices(int index, int (&indices)[3])const{
    indices[0] =  index % nCells_[0];
    indices[1] = (index / nCells_[0]) % nCells_[1];
    indices[2] =  index / (nCells_[0] * nCells_[1]);
}

int BoopCellList::cellIndex(const clam::Vec3d& pos)const{
    int cellIndices[3];
    for(int i = 0; i < 3; ++i) cellIndices[i] = int(pos[i] / cellSize_[i]);
    return indicesToIndex(cellIndices);
}

clam::Vec3d BoopCellList::cell_origin(int cidx)const{
    return clam::Vec3d(
        cellSize_[0] * (cidx % nCells_[0]),
        cellSize_[1] * ((cidx / nCells_[0]) % nCells_[1]),
        cellSize_[2] * (cidx / (nCells_[0] * nCells_[1]))
    );
}
