# BOOP aka bond orientational order parameter

A C/C++ libary for calculating the (averaged) local bond order parameters, q, and w (see e.g. [Dellago et al. JCP 129(11) 2008](http://dx.doi.org/10.1063/1.2977970)).



# Compiling

A C++11 compatible compiler is needed. Run `make` at the root directory to compile the library, `liboop.so`.
When using BOOP, make sure to include `boop.h`, and link against the shared library `libboop.so`.

# Usage

To calculate the boops we first need to create a `BoopConfig`, which contains information for the calculations.
We call

```C
BoopConfig* boop_config_new(unsigned int n_part, double* positions, const double box_bounds[3], bool is_periodic, double range);
```

which takes the number of particles, an array of the particle positions, the box size, a flag which tells us if we are to use periodic boundary conditions,
and finally the cutoff range to be used in the calculations. Note that the particle positions should be defined such as to lie in the positive quadrant of the Cartesian
coordinate system. The `BoopConfig` can be deleted by calling,

```C
void boop_config_delete(BoopConfig*);
```

Finally, the individual order parameters can be calculated by calling the following,

```C
void boop_ql(const BoopConfig* config, int l, double* qls);
void boop_wl(const BoopConfig* config, int l, double* wls);
void boop_ql_av(const BoopConfig* config, int l, double* qls);
void boop_wl_av(const BoopConfig* config, int l, double* wls);
```

where the last argument is the array that is to be filled by the function. It should be at least `n_part` long.
