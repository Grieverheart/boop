#include "boop.h"
#include "boop_cell_list.h"
#include <cstdio>
#include <cstring>
#include <complex>
#include <vector>
#include <cassert>

struct BoopConfig{
    BoopConfig(unsigned int _n_part, double* _positions, clam::Vec3d _box_bounds, bool _is_periodic, double _range):
        n_part(_n_part), box_bounds(_box_bounds), range(_range), is_periodic(_is_periodic)
    {
        assert(range < box_bounds[0]); assert(range < box_bounds[1]); assert(range < box_bounds[2]);
        positions = new clam::Vec3d[n_part];
        memcpy(positions, _positions, 3 * n_part * sizeof(double));
        cll.init(n_part, box_bounds, 2.0 * range);
        for(unsigned int pid = 0; pid < n_part; ++pid){
            cll.add(pid, positions[pid]);
#ifndef NDEBUG
            for(unsigned int d = 0; d < 3; ++d){
                assert(positions[pid][d] >= 0.0);
                assert(positions[pid][d] <= bounds_bounds[d]);
            }
#endif
        }
    }

    ~BoopConfig(void){
        delete[] positions;
    }

    unsigned int n_part;
    clam::Vec3d* positions;
    clam::Vec3d box_bounds;
    double range;
    bool is_periodic;
    BoopCellList cll;
};

static inline clam::Vec3d minimum_image(const clam::Vec3d& dist, const clam::Vec3d& box_bounds){
    clam::Vec3d ret(dist);
    for(int i = 0; i < 3; ++i){
        ret[i] += (ret[i] > 0.5 * box_bounds[i])? -box_bounds[i]: (ret[i] <  -0.5 * box_bounds[i])?  box_bounds[i]: 0.0;
    }
    return ret;
}

static inline double factorial(size_t x){
    static double memo[] = {
        1.0, 1.0, 2.0, 6.0, 24.0, 120.0, 720.0, 5040.0, 40320.0,
        362880.0, 3628800.0, 39916800.0, 479001600.0, 6227020800.0,
        87178291200.0, 1307674368000.0, 20922789888000.0
    };
    assert(x < 170);
    return (size_t(x) < sizeof(memo) / sizeof(double))? memo[x]: x * factorial(x - 1);
}

static inline double binomial_coefficient(int n, int k){
    return factorial(n) / (factorial(k) * factorial(n - k));
}

static inline double falling_factorial(int x, int m){
    return binomial_coefficient(x, m) * factorial(m);
}

static inline double ipow_helper(double base, size_t exponent){
    return (exponent)? base * ipow_helper(base, exponent - 1): 1.0;
}

static inline double ipow(double base, int exponent){
    return (exponent > 0)? ipow_helper(base, exponent): 1.0 / ipow_helper(base, -exponent);
}

static inline double legendre_polynomial(int l, int m, double x){
	int mm;
	double factor;
	if(m < 0){
		mm = -m;
		factor = (factorial(l - mm) / factorial(l + mm)) * ipow(sqrt(1 - x * x), mm) / (1 << l);
	}
	else{
		mm = m;
		factor = ipow(-sqrt(1 - x * x), mm) / (1 << l);
	}
	
	double sum_part = 0.0;
	for(int k = 0; k <= l / 2; ++k){
		if(l - 2 * k < mm) break;
		double bin = binomial_coefficient(l, k) * binomial_coefficient(2 * (l - k), l);
		int sign = ((k & 1) == 0)? 1 : -1;
		sum_part += sign * falling_factorial((l - 2 * k), mm) * bin * ipow(x, l - 2 * k - mm);
	}

	return factor * sum_part;
}

static inline std::complex<double> spherical_harmonic(int l, int m, double cos_theta, double phi){
	double factor = sqrt((2.0 * l + 1) * factorial(l - m) / (factorial(l + m) * 4.0 * M_PI));
	factor *= legendre_polynomial(l, m, cos_theta);
    return factor * std::complex<double>(cos(m * phi), sin(m * phi));
}

static void ql(unsigned int n_part, const clam::Vec3d* positions, clam::Vec3d box_bounds, const BoopCellList& cll, bool is_periodic, double range, int l, double* qls){
    const double factor = 4.0 * M_PI / (2.0 * l + 1.0);

    for(size_t pi = 0; pi < n_part; ++pi){
        int cidx = cll.cell_index(pi);
        qls[pi] = 0.0;
        int nbs = 0;
        std::complex<double> qlms[2 * l + 1]{};
        for(auto cell: cll.cell_nbs(cidx)){
            for(size_t pj: cll.cell_content(cell)){
                if(pi == pj) continue;
                auto dist = (is_periodic)? minimum_image(positions[pj] - positions[pi], box_bounds): positions[pj] - positions[pi];
                if(dist.length2() < range * range){
                    ++nbs;
                    double cos_theta = dist[2] / dist.length();
                    double phi   = atan2(dist[1], dist[0]);
                    for(int m = -l; m < l + 1; ++m){
                        qlms[m + l] += spherical_harmonic(l, m, cos_theta, phi);
                    }
                }
            }
        }
        if(nbs > 0){
            for(int m = -l; m < l + 1; ++m){
                qls[pi] += std::norm(qlms[m + l] / double(nbs));
            }
            qls[pi] = sqrt(factor * qls[pi]);
        }
    }
}

static void qlav(unsigned int n_part, const clam::Vec3d* positions, clam::Vec3d box_bounds, const BoopCellList& cll, bool is_periodic, double range, int l, double* qls){
    const double factor = 4.0 * M_PI / (2.0 * l + 1.0);

    std::complex<double>* qlms = new std::complex<double>[(2 * l + 1) * n_part]{};
    std::vector<int>* neighbours = new std::vector<int>[n_part];
    for(size_t pi = 0; pi < n_part; ++pi){
        int cidx = cll.cell_index(pi);
        int nbs = 0;
        for(auto cell: cll.cell_nbs(cidx)){
            for(size_t pj: cll.cell_content(cell)){
                if(pi == pj) continue;
                auto dist = (is_periodic)? minimum_image(positions[pj] - positions[pi], box_bounds): positions[pj] - positions[pi];
                if(dist.length2() < range * range){
                    ++nbs;
                    neighbours[pi].push_back(pj);
                    double cos_theta = dist[2] / dist.length();
                    double phi = atan2(dist[1], dist[0]);
                    for(int m = -l; m < l + 1; ++m){
                        qlms[(2 * l + 1) * pi + m + l] += spherical_harmonic(l, m, cos_theta, phi);
                    }
                }
            }
        }
        if(nbs){
            for(int m = -l; m < l + 1; ++m) qlms[(2 * l + 1) * pi + m + l] /= double(nbs);
        }
    }

    std::complex<double>* qlms_av = new std::complex<double>[(2 * l + 1) * n_part];
    for(size_t i = 0; i < (2 * l + 1) * n_part; ++i) qlms_av[i] = qlms[i];

    for(size_t pi = 0; pi < n_part; ++pi){
        for(auto pj: neighbours[pi]){
            for(int m = -l; m < l + 1; ++m){
                qlms_av[(2 * l + 1) * pi + m + l] += qlms[(2 * l + 1) * pj + m + l];
            }
        }
        for(int m = -l; m < l + 1; ++m) qlms_av[(2 * l + 1) * pi + m + l] /= double(neighbours[pi].size() + 1);
    }

    delete[] qlms;
    delete[] neighbours;

    for(size_t pi = 0; pi < n_part; ++pi){
        qls[pi] = 0.0;
        for(int m = -l; m < l + 1; ++m){
            qls[pi] += std::norm(qlms_av[(2 * l + 1) * pi + m + l]);
        }
        qls[pi] = sqrt(factor * qls[pi]);
    }

    delete[] qlms_av;
}

static void wl(unsigned int n_part, const clam::Vec3d* positions, clam::Vec3d box_bounds, const BoopCellList& cll, bool is_periodic, double range, int l, double* wls){
    double prefactor = factorial(l);
    prefactor *= prefactor * prefactor;
    prefactor /= factorial(3 * l + 1);

    for(size_t pi = 0; pi < n_part; ++pi){
        wls[pi] = 0.0;
        int cidx = cll.cell_index(pi);
        //Calculate qlms
        double qlms_norm = 0.0;
        std::complex<double> qlms[2 * l + 1]{};
        int nbs = 0;
        for(auto cell: cll.cell_nbs(cidx)){
            for(size_t pj: cll.cell_content(cell)){
                if(pi == pj) continue;
                auto dist = (is_periodic)? minimum_image(positions[pj] - positions[pi], box_bounds): positions[pj] - positions[pi];
                if(dist.length2() < range * range){
                    ++nbs;
                    double cos_theta = dist[2] / dist.length();
                    double phi   = atan2(dist[1], dist[0]);
                    for(int m = -l; m < l + 1; ++m){
                        qlms[m + l] += spherical_harmonic(l, m, cos_theta, phi);
                    }
                }
            }
        }
        if(nbs > 0){
            for(int m = -l; m < l + 1; ++m){
                qlms[m + l] /= double(nbs);
                qlms_norm += std::norm(qlms[m + l]);
            }
            qlms_norm = sqrt(qlms_norm * qlms_norm * qlms_norm);
        }

        for(int m1 = -l; m1 < l + 1; ++m1){
            for(int m2 = -l; m2 < l + 1; ++m2){
                for(int m3 = -l; m3 < l + 1; ++m3){
                    if(m1 + m2 + m3 != 0) continue;
                    double wigner = ((std::abs(m3) % 2 == 0)? 1: -1) * sqrt(prefactor *
                        factorial(l - m1) * factorial(l + m1) *
                        factorial(l - m2) * factorial(l + m2) *
                        factorial(l - m3) * factorial(l + m3));
                    double sum = 0.0;
                    for(int k = std::max(0, std::max(-m1, m2)); k <= std::min(l, std::min(-m1, m2) + l); ++k){
                        sum += 1.0 / (((k % 2 == 0)? 1: -1) *
                               factorial(k) * factorial(l - k) *
                               factorial(l - m1 - k) * factorial(l + m2 - k) *
                               factorial(m1 + k) * factorial(-m2 + k));
                    }

                    wigner = wigner * sum;
                    wls[pi] += wigner * (qlms[m1 + l] * qlms[m2 + l] * qlms[m3 + l]).real();
                }
            }
        }
        wls[pi] /= qlms_norm;
    }
}

static void wlav(unsigned int n_part, const clam::Vec3d* positions, clam::Vec3d box_bounds, const BoopCellList& cll, bool is_periodic, double range, int l, double* wls){
    std::complex<double>* qlms = new std::complex<double>[(2 * l + 1) * n_part]{};
    std::vector<int>* neighbours = new std::vector<int>[n_part];
    for(size_t pi = 0; pi < n_part; ++pi){
        int cidx = cll.cell_index(pi);
        int nbs = 0;
        for(auto cell: cll.cell_nbs(cidx)){
            for(size_t pj: cll.cell_content(cell)){
                if(pi == pj) continue;
                auto dist = (is_periodic)? minimum_image(positions[pj] - positions[pi], box_bounds): positions[pj] - positions[pi];
                if(dist.length2() < range * range){
                    ++nbs;
                    neighbours[pi].push_back(pj);
                    double cos_theta = dist[2] / dist.length();
                    double phi = atan2(dist[1], dist[0]);
                    for(int m = -l; m < l + 1; ++m){
                        qlms[(2 * l + 1) * pi + m + l] += spherical_harmonic(l, m, cos_theta, phi);
                    }
                }
            }
        }
        if(nbs){
            for(int m = -l; m < l + 1; ++m) qlms[(2 * l + 1) * pi + m + l] /= double(nbs);
        }
    }

    double prefactor = factorial(l);
    prefactor *= prefactor * prefactor;
    prefactor /= factorial(3 * l + 1);

    for(size_t pi = 0; pi < n_part; ++pi){
        wls[pi] = 0.0;
        //Calculate qlms
        std::complex<double> qlms_av[2 * l + 1];
        for(auto pj: neighbours[pi]){
            for(int m = -l; m < l + 1; ++m){
                qlms_av[l + m] += qlms[(2 * l + 1) * pj + m + l];
            }
        }

        double qlms_norm = 0.0;
        for(int m = -l; m < l + 1; ++m){
            qlms_av[m + l] += qlms[(2 * l + 1) * pi + m + l];
            qlms_av[m + l] /= double(neighbours[pi].size() + 1);
            qlms_norm += std::norm(qlms_av[m + l]);
        }

        qlms_norm = sqrt(qlms_norm * qlms_norm * qlms_norm);

        for(int m1 = -l; m1 < l + 1; ++m1){
            for(int m2 = -l; m2 < l + 1; ++m2){
                for(int m3 = -l; m3 < l + 1; ++m3){
                    if(m1 + m2 + m3 != 0) continue;
                    double wigner = ((std::abs(m3) % 2 == 0)? 1: -1) * sqrt(prefactor *
                        factorial(l - m1) * factorial(l + m1) *
                        factorial(l - m2) * factorial(l + m2) *
                        factorial(l - m3) * factorial(l + m3));
                    double sum = 0.0;
                    for(int k = std::max(0, std::max(-m1, m2)); k <= std::min(l, std::min(-m1, m2) + l); ++k){
                        sum += 1.0 / (((k % 2 == 0)? 1: -1) *
                               factorial(k) * factorial(l - k) *
                               factorial(l - m1 - k) * factorial(l + m2 - k) *
                               factorial(m1 + k) * factorial(-m2 + k));
                    }

                    wigner = wigner * sum;
                    wls[pi] += wigner * (qlms_av[m1 + l] * qlms_av[m2 + l] * qlms_av[m3 + l]).real();
                }
            }
        }
        wls[pi] /= qlms_norm;
    }

    delete[] qlms;
    delete[] neighbours;
}

BoopConfig* boop_config_new(unsigned int n_part, double* positions, const double box_bounds[3], bool is_periodic, double range){
    return new BoopConfig(n_part, positions, box_bounds, is_periodic, range);
}

void boop_config_delete(BoopConfig* config){
    delete config;
}


void boop_ql(const BoopConfig* config, int l, double* qls){
    assert(l > 0);
    ql(config->n_part, config->positions, config->box_bounds, config->cll, config->is_periodic, config->range, l, qls);
}

void boop_wl(const BoopConfig* config, int l, double* wls){
    assert(l > 0);
    wl(config->n_part, config->positions, config->box_bounds, config->cll, config->is_periodic, config->range, l, wls);
}

void boop_ql_av(const BoopConfig* config, int l, double* qls){
    assert(l > 0);
    qlav(config->n_part, config->positions, config->box_bounds, config->cll, config->is_periodic, config->range, l, qls);
}

void boop_wl_av(const BoopConfig* config, int l, double* wls){
    assert(l > 0);
    wlav(config->n_part, config->positions, config->box_bounds, config->cll, config->is_periodic, config->range, l, wls);
}

