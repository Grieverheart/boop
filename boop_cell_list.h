#ifndef BOOP_CELL_LIST_H
#define BOOP_CELL_LIST_H

#include "clam.h"

#define CLL_EMPTY -1

class BoopCellList{
public:
    BoopCellList(void);
    ~BoopCellList(void);

    BoopCellList(const BoopCellList&);

    void init(int nPart, clam::Vec3d box_bounds, double minCellSize);
    int add(int pid, const clam::Vec3d& pos);
    int add(int pid, int cidx);
    int update(int pid, int cidx);
    int update(int pid, const clam::Vec3d& pos);
    int move(int pid, int coffset);
    void rescale(const clam::Vec3d& factor);
    void clear(void);

    bool should_rebuild(const clam::Vec3d& factor)const;
    int cell_index(int pid)const;
    int cell_index_at_offset(int cid, int offset)const;
    clam::Vec3d cell_size(void)const;
    clam::Vec3d cell_origin(int cid)const;
    double cell_min_size(void)const;

    class CellIterator;
    class CellNeighbourIterator;
    class CellContentIterator;

    CellIterator          cells(void)const;
    CellNeighbourIterator cell_vol_nbs(int pid)const;
    CellNeighbourIterator cell_nbs(int cid)const;
    CellNeighbourIterator particle_cell_nbs(int pid)const;
    CellNeighbourIterator particle_cell_nbs(const clam::Vec3d& pos)const;
    CellNeighbourIterator cell_dir_nbs(int cid, int dir)const;
    CellContentIterator   cell_content(int cid)const;

    //NOTE: Also pushing the colliding particle to the front seems to
    //increase performance slightly, at least for the dense system we
    //checked using NVT simulations.
    void push_front(const CellNeighbourIterator&);
    int n_cells(void)const;

    int cellIndex(const clam::Vec3d& pos)const;
private:
    int indicesToIndex(const int (&indices)[3])const;
    void indexToIndices(int index, int (&indices)[3])const;

private:
    int    nPart_;
    int    nCells_[3];         //Number of cells in each dimension
    int    nCellsTot_;      //Total Number of cells
    int*   cell_;           //First particle in cell
    int*   linkedList_;
    int*   pCellIds_;
    int*   cellNeighbours_;
    int*   cellDirNeighbours_;
    int*   cellVolNeighbours_;
    clam::Vec3d cellSize_;       //Cell size in each dimension
    double cellMinSize_;
};

    class BoopCellList::CellNeighbourIterator{
        friend class BoopCellList;
    public:
        constexpr CellNeighbourIterator(const int* neighbour_list, int n_neighbours = 27):
            cellNeighbours_(neighbour_list),
            nidx_(0), end_(n_neighbours)
        {}

        constexpr CellNeighbourIterator begin(void)const{
            return CellNeighbourIterator(cellNeighbours_, end_);
        }

        CellNeighbourIterator end(void)const{
            CellNeighbourIterator ret = *this;
            ret.nidx_ = end_;
            return ret;
        }

        constexpr bool operator!=(const CellNeighbourIterator& other)const{
            return nidx_ != other.nidx_;
        }

        CellNeighbourIterator& operator++(void){
            ++nidx_;
            return *this;
        }

        constexpr int operator*(void)const{
            return cellNeighbours_[nidx_];
        }
    private:
        const int* cellNeighbours_;
        int nidx_;
        int end_;
    };

    class BoopCellList::CellIterator{
    public:
        constexpr CellIterator(int ncells):
            ncells_(ncells), cid_(0)
        {}

        constexpr CellIterator begin(void)const{
            return CellIterator(ncells_);
        }

        CellIterator end(void)const{
            CellIterator ret = *this;
            ret.cid_ = ncells_;
            return ret;
        }

        constexpr bool operator!=(const CellIterator& other)const{
            return cid_ != other.cid_;
        }

        CellIterator& operator++(void){
            ++cid_;
            return *this;
        }

        constexpr int operator*(void)const{
            return cid_;
        }
    private:
        int ncells_;
        int cid_;
    };

    class BoopCellList::CellContentIterator{
    public:
        constexpr CellContentIterator(const BoopCellList& parent, int cidx):
            first_pid(parent.cell_[cidx]), curr_pid(parent.cell_[cidx]),
            linkedList_(parent.linkedList_)
        {}

        CellContentIterator begin(void)const{
            CellContentIterator ret = *this;
            ret.curr_pid = first_pid;
            return ret;
        }

        CellContentIterator end(void)const{
            CellContentIterator ret = *this;
            ret.curr_pid = CLL_EMPTY;
            return ret;
        }

        constexpr bool operator!=(const CellContentIterator& other)const{
            return curr_pid != other.curr_pid;
        }

        CellContentIterator& operator++(void){
            curr_pid = linkedList_[curr_pid];
            return *this;
        }

        CellContentIterator operator+(int num)const{
            auto ret = *this;
            for(int i = 0; i < num; ++i) ret.curr_pid = ret.linkedList_[ret.curr_pid];
            return ret;
        }

        constexpr int operator*(void)const{
            return curr_pid;
        }
    private:
        int first_pid;
        int curr_pid;
        const int* linkedList_;
    };
#endif
